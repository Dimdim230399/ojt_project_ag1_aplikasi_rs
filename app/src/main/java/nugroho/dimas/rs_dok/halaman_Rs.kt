package nugroho.dimas.rs_dok

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_halaman_rs.*
import org.json.JSONArray

class halaman_Rs : AppCompatActivity(), View.OnClickListener {

   // var url = "http://192.168.43.156/coba_Laravel7/public/RS/getData"
   //var url ="http://192.168.43.126/dashboard/coba_Laravel7/public/RS/getData"
    var url ="http://192.168.43.188/ojt_RS_api/public/getDataRS"
    var url1 = "http://192.168.43.188/ojt_RS_api/public/getNamaKota"
    var url2 = "http://192.168.43.188/ojt_RS_api/public/getNamaKotaku"


    var dataRs =  mutableListOf<HashMap<String,String>>()
    lateinit var adapterRs: AdapterRs

    var daftarkota = mutableListOf<String>()
    lateinit var Adapkota : ArrayAdapter<String>

    var datakota = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_halaman_rs)

        adapterRs = AdapterRs(dataRs,this)
        rcRs.layoutManager =GridLayoutManager(this,2)
        rcRs.adapter = adapterRs
        crRs.setOnClickListener(this)

        Adapkota = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarkota)



    }

    override fun onStart() {
        super.onStart()
        ShowRs("","")
        showKota()
    }


    fun showKota(){
        val request  = StringRequest(Request.Method.POST,url1,
            Response.Listener { response ->
                daftarkota.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                        daftarkota.add(jsonObject.getString("nama_kota"))
                }
                Adapkota.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->

            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    val itemselect = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            datakota = daftarkota.get(0)

        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            datakota = daftarkota.get(position)

        }

    }


    fun ShowRs(namaRs : String , namaKota : String){
        val request = object : StringRequest(
            Request.Method.POST,url, Response.Listener { response ->
                dataRs.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var info = HashMap<String,String>()
                    info.put("nm_Rs",jsonObject.getString("RS_NAMA"))
                    info.put("no_telp",jsonObject.getString("RS_TELP"))
                    info.put("alamat",jsonObject.getString("RS_ALAMAT"))
                    info.put("url",jsonObject.getString("url"))
                    info.put("profil",jsonObject.getString("RS_PROFIL"))
                    info.put("kota",jsonObject.getString("nama_kota"))
                    dataRs.add(info)
                }
                adapterRs.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
            }){ override fun getParams(): MutableMap<String, String> {
            val hm = HashMap<String,String>()
            hm.put("nama_rs",namaRs)
            return hm
        }
        }
        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }



    override fun onClick(v: View?) {
        when(v?.id){
            R.id.crRs->{
                ShowRs(inpRs.text.toString().trim(),"")
            }
        }

    }
}
