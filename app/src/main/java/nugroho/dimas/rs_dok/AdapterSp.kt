package nugroho.dimas.rs_dok

import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterSp  (val dataSpe : List<HashMap<String,String>>, val halamanDoc: halaman_doc) : RecyclerView.Adapter<AdapterSp.HolderSp> (){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterSp.HolderSp {
        val v  = LayoutInflater.from(parent.context).inflate(R.layout.isi_spesialis,parent,false)
        return HolderSp(v)
    }

    override fun getItemCount(): Int {
        return dataSpe.size
    }

    override fun onBindViewHolder(holder: AdapterSp.HolderSp, position: Int) {
        val data = dataSpe.get(position)
        holder.txNamaRs.setText(data.get("nm_sp"))

        if (position.rem(2) == 0) holder.car.setBackgroundColor(Color.rgb(230,245,240))
        else holder.car.setBackgroundColor(Color.rgb(255,255,245))
        val set1: Animation = AnimationUtils.loadAnimation(this.halamanDoc, R.anim.rcanim)
        holder.car.startAnimation(set1)


        holder.car.setOnClickListener {
            var aa = dataSpe.get(position).get("nm_sp")
            Toast.makeText(this.halamanDoc ,aa ,Toast.LENGTH_LONG).show()
            val det =  Intent(this.halamanDoc, list_doc_sp::class.java)
            det.putExtra("nama",dataSpe.get(position).get("nm_sp"))
            this.halamanDoc.startActivity(det)

        }

    }


    class HolderSp(v : View) : RecyclerView.ViewHolder(v){
        val txNamaRs = v.findViewById<TextView>(R.id.tvNamaSp)
        val  car = v.findViewById<CardView>(R.id.CardSp)
    }


}