package nugroho.dimas.rs_dok

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment

class dialogpesan : AppCompatDialogFragment() {
    lateinit var v : View
    lateinit var  dialog : AlertDialog.Builder
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        dialog = activity?.let { AlertDialog.Builder(it) }!!
        v = layoutInflater.inflate(R.layout.isi_pesan,null)
        dialog.setView(v)
            .setTitle("Hubungi Dokter")
            .setNegativeButton("Batal",  tidak)
            .setNegativeButton("kirim", iya)
        return dialog.create()

    }

    val  iya = DialogInterface.OnClickListener { dialog, which ->

    }

    val  tidak = DialogInterface.OnClickListener { dialog, which ->

    }
}