package nugroho.dimas.rs_dok

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_profil_rs.*

class profil_rs : AppCompatActivity() , View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profil_rs)

        var aa = intent
        var nama = aa.getStringExtra("nama")
        var image = aa.getStringExtra("foto")
        var notelp = aa.getStringExtra("notelp")
        var prof = aa.getStringExtra("profil")
        var alamat = aa.getStringExtra("alamat")
        var kota = aa.getStringExtra("kota")

        nmRs.setText(nama)
        Picasso.get().load(image).into(imgtv)
        ProfilRs.setText(prof)
        almRs.setText("$alamat")
        notelphone.setText(notelp)
        showDoc.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.showDoc->{
                var a = Intent(this,list_doc_rs::class.java)
                a.putExtra("namaRs",nmRs.text.toString())
                startActivity(a)
            }
        }
    }
}
