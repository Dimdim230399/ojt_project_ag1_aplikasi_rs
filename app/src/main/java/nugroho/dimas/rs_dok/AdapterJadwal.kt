package nugroho.dimas.rs_dok

import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterJadwal(val datajad : List<HashMap<String,String>>, val halamanRs: profil_doc) : RecyclerView.Adapter<AdapterJadwal.HolderRs> (){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterJadwal.HolderRs {
        val v  = LayoutInflater.from(parent.context).inflate(R.layout.isi_jadwal,parent,false)
        return HolderRs(v)
    }

    override fun getItemCount(): Int {
        return datajad.size
    }

    override fun onBindViewHolder(holder: AdapterJadwal.HolderRs, position: Int) {
        val data = datajad.get(position)
        var waktu = data.get("wak")
        var jm = data.get("jm")
        holder.txWal.setText("$waktu, $jm")
//        holder.txalm.setText("$kot,$alm")
//        if (!data.get("url").equals(""))
//            Picasso.get().load(data.get("url")).into(holder.gam)

        if (position.rem(2) == 0) holder.car.setBackgroundColor(Color.rgb(230,245,240))
        else holder.car.setBackgroundColor(Color.rgb(255,255,245))
        val set1: Animation = AnimationUtils.loadAnimation(this.halamanRs, R.anim.rcanim)
        holder.car.startAnimation(set1)

//
//        holder.car.setOnClickListener {
//            val det =  Intent(this.halamanRs, profil_rs::class.java)
//            det.putExtra("nama",dataRs.get(position).get("nm_Rs"))
//            det.putExtra("notelp",dataRs.get(position).get("no_telp"))
//            det.putExtra("alamat",dataRs.get(position).get("alamat"))
//            det.putExtra("foto",dataRs.get(position).get("url"))
//            det.putExtra("kot",dataRs.get(position).get("kota"))
//            det.putExtra("profil",dataRs.get(position).get("profil"))
//            this.halamanRs.startActivity(det)
//        }

    }


    class HolderRs(v : View) : RecyclerView.ViewHolder(v){
        val txWal = v.findViewById<TextView>(R.id.wal)
//        val txalm = v.findViewById<TextView>(R.id.tvAlm)
//        val gam=v.findViewById<ImageView>(R.id.gam)
        val  car = v.findViewById<CardView>(R.id.jad)
    }


}