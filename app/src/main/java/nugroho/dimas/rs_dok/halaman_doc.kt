package nugroho.dimas.rs_dok

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_halaman_doc.*
import org.json.JSONArray

class halaman_doc : AppCompatActivity() {
    var url ="http://192.168.43.188/ojt_RS_api/public/getNamaSpesialis"
//   var url ="http://192.168.43.126/dashboard/coba_Laravel7/public/RS/getNamaSpesialis"

    var dataSp =  mutableListOf<HashMap<String,String>>()
    lateinit var adapterSp: AdapterSp

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_halaman_doc)

        adapterSp = AdapterSp(dataSp,this)
        rcPildoc.layoutManager = LinearLayoutManager(this)
        rcPildoc.adapter = adapterSp
    }

    override fun onStart() {
        super.onStart()
        ShowSp("")
    }

    fun ShowSp(namaRs : String){
        val request = object : StringRequest(
            Request.Method.POST,url, Response.Listener { response ->
                dataSp.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var info = HashMap<String,String>()
                    info.put("nm_sp",jsonObject.getString("SP_NAMA"))
                    dataSp.add(info)
                }
                adapterSp.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
            }){ override fun getParams(): MutableMap<String, String> {
            val hm = HashMap<String,String>()
            hm.put("nama_rs",namaRs)
            return hm
        }
        }
        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }

}
