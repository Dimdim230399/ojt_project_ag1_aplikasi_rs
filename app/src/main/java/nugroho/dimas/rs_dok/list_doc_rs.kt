package nugroho.dimas.rs_dok

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_list_doc_rs.*
import kotlinx.android.synthetic.main.activity_list_doc_sp.*
import kotlinx.android.synthetic.main.activity_list_doc_sp.rcPildocSp
import org.json.JSONArray

class list_doc_rs : AppCompatActivity() {

    //var url = "http://192.168.43.156/coba_Laravel7/public/dokter/getDataDokter"
    var url ="http://192.168.43.188/ojt_RS_api/public/getDataDokter"
    var dataDoc = mutableListOf<HashMap<String,String>>()
    lateinit var AdapterDocRs : AdapterDocRs
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_doc_rs)
        AdapterDocRs = AdapterDocRs(dataDoc,this)
        rcPildocSp.layoutManager = GridLayoutManager(this,2)
        rcPildocSp.adapter = AdapterDocRs


        var aa = intent
        var nama = aa.getStringExtra("namaRs")
txRs.setText(nama)
        ShowDok(nama)


    }

    fun ShowDok(namaSp : String){
        val request = object : StringRequest(
            Request.Method.POST,url, Response.Listener { response ->
                dataDoc.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var info = HashMap<String,String>()
                    info.put("nm_doc",jsonObject.getString("DOKTER_NAMA"))
                    info.put("sp_nm",jsonObject.getString("nama_sp"))
                    info.put("alamat",jsonObject.getString("RS_NAMA"))
                    info.put("url",jsonObject.getString("DOKTER_GBR"))
                    info.put("profil",jsonObject.getString("DOKTER_PROFIL"))
                    info.put("dokstr",jsonObject.getString("DOKTER_STR"))
                    info.put("doc_hp",jsonObject.getString("DOKTER_HP"))
                    info.put("rs_gam",jsonObject.getString("url"))
                    dataDoc.add(info)
                }
                AdapterDocRs.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
            }){ override fun getParams(): MutableMap<String, String> {
            val hm = HashMap<String,String>()
            hm.put("nama_rs",namaSp)
            return hm
        }
        }
        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }




}
