package nugroho.dimas.rs_dok

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_profil_doc.*
import kotlinx.android.synthetic.main.activity_profil_doc.imgtv
import kotlinx.android.synthetic.main.activity_profil_doc.notelphone
import kotlinx.android.synthetic.main.isi_pesan.view.*
import org.json.JSONArray

class profil_doc : AppCompatActivity() , View.OnClickListener {

    var url = "http://192.168.43.188/ojt_RS_api/public/getJadwalDokter"
    var url1 = "http://192.168.43.188/ojt_RS_api/public/postInsHistory"
    var dataJad = mutableListOf<HashMap<String,String>>()
    val nama = "nama"
    val keluhan = "keluhan"
    val nm_doc = "nama_dokter"
    lateinit var AdapterJadwal : AdapterJadwal
    lateinit var  dialogpesan: dialogpesan
    override fun onCreate(savedInstanceState: Bundle?)  {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profil_doc)

        AdapterJadwal = AdapterJadwal(dataJad,this)
        rcjadwal.layoutManager = LinearLayoutManager(this)
        rcjadwal.adapter = AdapterJadwal



        var aa = intent
        var nama = aa.getStringExtra("nama")
        var spesialis = aa.getStringExtra("spesialis")
        var strdoc = aa.getStringExtra("str")
        var rsnam = aa.getStringExtra("nama_rs")
        var profil = aa.getStringExtra("profil_doc")
        var fotodoc = aa.getStringExtra("foto_doc")
        var hp = aa.getStringExtra("nohp")
        var fotRs = aa.getStringExtra("foto_rs")

        Picasso.get().load(fotodoc).into(foto)
        Picasso.get().load(fotRs).into(imgtv)
        nmDoc.setText(nama)
        Profildoc.setText(profil)
        str.setText(strdoc)
        notelphone.setText(hp)
        spDoc.setText(spesialis)
        nmRsDoc.setText(rsnam)
        callDoc.setOnClickListener(this)
        btHome.setOnClickListener(this)

        ShowDok(nmDoc.text.toString())

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.callDoc->{
                val viewDial = LayoutInflater.from(this).inflate(R.layout.isi_pesan,null)
                val dialog  = AlertDialog.Builder(this)
                    .setView(viewDial)
                    .setTitle("Call Dokter")
                val LihatDial = dialog.show()

                viewDial.dok.setText(nmDoc.text.toString())
                viewDial.panggil.setOnClickListener {
                    LihatDial.dismiss()

                    var namaPS = viewDial.psNam.text.toString()
                    var alamtPS =viewDial.psAlm.text.toString()
                    var keluhanPS = viewDial.psKeluh.text.toString()
                    var umurPs = viewDial.psUmur.text.toString()

                    val request = object : StringRequest(Method.POST,url1, Response.Listener { response ->
                    },
                        Response.ErrorListener { error ->
                        Toast.makeText(this,"Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
                    }) {
                        override fun getParams(): MutableMap<String, String> {
                            val hm = HashMap<String, String>()
                            hm.put(nama, namaPS)
                            hm.put(keluhan,keluhanPS)
                            hm.put(nm_doc, nmDoc.text.toString())
                            return hm
                        }
                    }
                    val queue =  Volley.newRequestQueue(this)
                    queue.add(request)
                    var aa ="https://api.whatsapp.com/send?phone=62895396119349&text=~*Hai*%20*"+nmDoc.text.toString()+"*%20~%0A%0APerkenalkan%20%0ANama%20%20%20%3A%20"+namaPS+"%0AUmur%20%20%20%20%3A%20"+umurPs+"%0AAlamat%20%20%3A%20"+alamtPS+"%0A%0Asaya%20mau%20menanyakan%20tentang%20keluhan%20saya%20yaitu%20%3A%0A"+keluhanPS+"%0A%0A_Terima%20Kasih_%0A%23projectAppOjt_Ag01%20%20"
                    val bel = Intent(Intent.ACTION_VIEW)
                    bel.setData(Uri.parse("https://api.whatsapp.com/send?phone="+notelphone.text.toString()+"&text=~*Hai*%20*"+nmDoc.text.toString()+"*%20~%0A%0APerkenalkan%20%0ANama%20%20%20%3A%20"+namaPS+"%0AUmur%20%20%20%20%3A%20"+umurPs+"%0AAlamat%20%20%3A%20"+alamtPS+"%0A%0Asaya%20mau%20menanyakan%20tentang%20keluhan%20saya%20yaitu%20%3A%0A"+keluhanPS+"%0A%0A_Terima%20Kasih_%0A%23projectAppOjt_Ag01%20%20"
                    ))
                    startActivity(bel)

                }

                viewDial.batal.setOnClickListener {
                    LihatDial.dismiss()
                }
            }
            R.id.btHome->{
                    var aa = Intent(this , Laman_mainmenu::class.java)
                    startActivity(aa)
        }
        }

    }
    fun ShowDok(namaDoc : String){
        val request = object : StringRequest(
            Request.Method.POST,url, Response.Listener { response ->
                dataJad.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var info = HashMap<String,String>()
                    info.put("wak",jsonObject.getString("JADWAL_JAM_M"))
                    info.put("jm",jsonObject.getString("JADWAL_JAM_S"))
                    dataJad.add(info)
                }
                AdapterJadwal.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
            }){ override fun getParams(): MutableMap<String, String> {
            val hm = HashMap<String,String>()
            hm.put("nama_dokter",namaDoc)
            return hm
        }
        }
        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }


}
